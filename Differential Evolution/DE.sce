mode(-1);
clear;
clc;

// Pontos experimentais
Ttab = [114.85 112.05 110.25 109 105.40 104.30 102.30 100.90 95.40 93.20 91.40 88.90 88 86.30 84.60];
Xtab = [0.046 0.11 0.138 0.173 0.246 0.26 0.305 0.337 0.474 0.573 0.616 0.710 0.745 0.816 0.9];
Ytab = [0.90991 0.90758 0.91723 0.91855 0.92893 0.95695 0.96777 0.97695 1.03170 0.99106 1.02445 1.00954 1.00926 1.00704 0.99812];

// Constantes
v = [94.88; 91.97];

R = 1.987;   // Constante universal dos gases
P = 760;     // Pressão atmosférica em mmHg
C2K = 273.15; // Conversão Celsius para Kelvin

A = [7.1489; 7.838];
B = [989; 1558];
C = [148.184; 196.881];

function [gc] = gama_calc(theta, x, t)
    x(2) = 1 - x(1);
    
    la1 = (v(2)/v(1)) * exp(-theta(1)/(R*(t+C2K)));
    la2 = (v(1)/v(2)) * exp(-theta(2)/(R*(t+C2K)));
    
    a = (x(1) + la1*x(2));
    b = (x(2) + la2*x(1));
    
    gc(1) = exp(+ x(2) * (la1/a - la2/b)) / a;
    gc(2) = exp(- x(1) * (la1/a - la2/b)) / b;
endfunction

function [ge] = gama_exp(x,y,t)
    x(2) = 1 - x(1);
    
    Psat = 10^(A - B./((t+C2K) + C));
    ge = (P*y)./(x.*Psat);
endfunction

function [y] = fn_fitness(X)
    y = 0;
    for i = 1:length(Xtab)
        gc = gama_calc(X, Xtab(i), Ttab(i));
        ge = gama_exp(Xtab(i), Ytab(i), Ttab(i));
        y = y + ((gc(1) - ge(1))/ge(1))^2 + ((gc(2) - ge(2))/ge(2))^2;
    end
endfunction

function [x, y] = DE(D, NP, CR, F, rng, f, gen_max)
    x = zeros(D, NP); // Matriz População Inicial
    y = zeros(D, NP); // Matriz População Mutante
    
    // Valores aleatórios para o intervalo [rng(1), rng(2)]
    for i = 1:NP
        x(:,i) = rng(1)+rand(D,1).*(rng(2)-rng(1));
    end
    
    for gen = 1:gen_max // Para cada geração
        for i = 1:NP // Percorre cada vetor da população
            // Seleciona 3 indices aleatorios
            while %T do r0 = ceil(rand()*NP); if ~(r0==i) then break end end
            while %T do r1 = ceil(rand()*NP); if ~(r1==i) & ~(r1==r0) then break end end
            while %T do r2 = ceil(rand()*NP); if ~(r2==i) & ~(r2==r0) & ~(r2==r1) then break end end
            jrand = ceil(D*rand()); // Assegura que pelo menos 1 variavel seja "mutada"
            
            for j = 1:D // gera o "trial vector"
                if rand() <= CR | j == jrand then
                    y(j,i) = x(j,r0) + F*(x(j,r1) - x(j, r2)); // "Muta" a variavel de indice j
                else
                    y(j,i) = x(j,i);   
                end
            end
        end
        
        for i = 1:NP // Seleciona qual "trial vector" deve ser passado para a próxima geração
            if f(y(:,i)) <= f(x(:,i)) then
                x(:,i) = y(:,i);
            end
        end
    end
    
    y = f(x);
endfunction

function start()
    [x,y] = DE(2, 20, 0.9, 0.5, [[-5000;5000][-5000;5000]], fn_fitness, 100);
    disp(x');
    disp(y');
    
    //plot3d(x(1,:),x(2,:),y);
endfunction
